import 'package:flutter/material.dart';
import 'package:flutter_framework/features/signup/presentation/screens/components/signup_form_group.dart';
import 'package:reactive_forms/reactive_forms.dart';

class SignupForm extends StatefulWidget {
  const SignupForm({super.key});

  @override
  State<SignupForm> createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  @override
  Widget build(BuildContext context) {
    final formContent = Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: ReactiveFormBuilder( 
        form: () => signupFormGroup,
        builder: (context, form, child) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ReactiveTextField(
              formControlName: 'username',
              decoration: const InputDecoration(
                labelText: 'Username',
              ),
            ),
            ReactiveTextField(
              formControlName: 'email',
              decoration: const InputDecoration(
                labelText: 'Email'
              ),
            ),
            ReactiveTextField(
              formControlName: 'phoneNumber',
              decoration: const InputDecoration(
                labelText: 'Phone Number'
              ),
            ),
            ReactiveTextField(
              formControlName: 'password',
              decoration: const InputDecoration(
                labelText: 'Password'
              ),
            ),
            ReactiveTextField(
              formControlName: 'passwordConformation',
              decoration: const InputDecoration(
                labelText: 'Conform Password'
              ),
            ),
            const SizedBox(height: 20),
            ReactiveFormConsumer(builder: ((context, formGroup, child) {
              return OutlinedButton(
                onPressed: formGroup.valid ? (){
                  print('hello');
                }:null, 
                child: const Text('Submit')
              );
            }))
          ],
        )),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
        centerTitle: true,
      ),
      body: formContent,
    );
  }
}