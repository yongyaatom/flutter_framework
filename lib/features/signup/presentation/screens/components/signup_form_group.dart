import 'package:reactive_forms/reactive_forms.dart';

final signupFormGroup = FormGroup({
    'username': FormControl<String> (
      validators: [
        Validators.required,
      ]
    ),
    'email': FormControl<String>(
      validators: [
        Validators.required,
        Validators.email,
      ]
    ),
    'phoneNumber': FormControl<int>(
      validators: [
        Validators.required,
        Validators.number
      ]
    ),
    'password': FormControl<String>(
      validators: [
        Validators.required,
        Validators.minLength(8),
      ]
    ),
    'passwordConformation': FormControl<String>()
  },
  validators: [
    Validators.mustMatch('password', 'passwordConformation', markAsDirty: false)
  ]
);