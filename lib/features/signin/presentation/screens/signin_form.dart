import 'package:flutter/material.dart';
import 'package:flutter_framework/features/signin/presentation/screens/components/signin_form_group.dart';
import 'package:reactive_forms/reactive_forms.dart';

class SigninForm extends StatefulWidget {
  const SigninForm({super.key});

  @override
  State<SigninForm> createState() => _SignupFormState();
}

class _SignupFormState extends State<SigninForm> {
  
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign In'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: ReactiveForm(
          formGroup: signinFormGroup,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ReactiveTextField(
                formControlName: 'email',
                decoration: const InputDecoration(
                  labelText: 'Email'
                ),
                // override default behavior and show errors when: INVALID, TOUCHED and DIRTY
                showErrors: (control) => control.invalid && control.touched && control.dirty,
              ),
              ReactiveTextField(
                formControlName: 'password',
                decoration: const InputDecoration(
                  labelText: 'Password'
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

final fbg = fb.group({
  'email': 'atomyongya@gmail.com'
}
);