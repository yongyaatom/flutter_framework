import 'package:reactive_forms/reactive_forms.dart';

final signinFormGroup = FormGroup({
  'email': FormControl<String>(
    validators: [
        Validators.required,
        Validators.composeOR([
          Validators.email,
          Validators.number,
        ],
      ),
    ],
  ),
  'password': FormControl<String>(
    validators: [
      Validators.required,
      Validators.email,
    ],
  ),
  },
);