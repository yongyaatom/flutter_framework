part of 'signin_form_bloc.dart';

abstract class SigninFormState extends Equatable {
  const SigninFormState();
  
  @override
  List<Object> get props => [];
}

class SigninFormInitial extends SigninFormState {}
