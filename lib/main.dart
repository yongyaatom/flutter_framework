import 'package:flutter/material.dart';
import 'package:flutter_framework/features/signup/presentation/screens/signup_form.dart';

void main(){
  runApp(const MyApp());
}

class MyApp extends StatefulWidget{
  const MyApp({Key? key}):super(key: key);

  @override
  State<MyApp> createState() => MyAppState(); 
}

class MyAppState extends State<MyApp>{
  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home: SignupForm(),
    );
  }
}

